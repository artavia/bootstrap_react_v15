
const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');

const app = express();

const config = require('./webpack.config.js');
const port = config.devServer.port; // 3000;
const hostname = config.devServer.host; // 'localhost';
const apppublicPath = config.output.publicPath;

const compiler = webpack(config);
const handlersObj = { publicPath: apppublicPath };
app.use( webpackDevMiddleware( compiler , handlersObj ) );

const cb = () => { console.log(`Example app listening at http://${hostname}:${port}`); };
app.listen( port , hostname, null, cb );

// https://webpack.js.org/guides/development
// https://webpack.js.org/configuration/devtool