"use strict";

const path = require("path");
const paths = {
  ROOT_DIR: path.resolve( __dirname ) // .
  , SRC_DIR: path.resolve( __dirname, 'src' ) // ./src
  , DIST_DIR: path.resolve( __dirname, 'dist' ) // ./dist
  , MODULES_DIR: path.resolve( __dirname, 'node_modules' ) // ./node_modules
};

const merge = require( 'webpack-merge' );
const common = require( './webpack.common.js' );

const webpack = require('webpack');

const wdefp = new webpack.DefinePlugin( {
  'process.env.NODE_ENV': JSON.stringify('development')
} );

const hotModuleDev = new webpack.HotModuleReplacementPlugin({});

const config = {
  
  devServer: {
    contentBase: paths.SRC_DIR
    , inline: true
    , historyApiFallback: true
    , hot: true
    , host: '127.0.0.1'
    , port: 8080
    , open: true
  } 

  , devtool: "inline-source-map"
  
  , plugins: [ hotModuleDev, wdefp ]
};

module.exports = merge(common,config);