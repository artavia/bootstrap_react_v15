
import './favicon.ico';
import './css/boots.scss';
import './js/index.js';

import React from 'react';
import { render } from 'react-dom'; // import { ReactDOM } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router'; 

import { Root } from './components/Root';
import { Home } from './components/Home';
import { About } from './components/About';
import { User } from './components/User';

class CustomApp extends React.Component {
  render(){
    return(
      <Router history={browserHistory}>
        <Route path={ '/' } component={Root} >
          <IndexRoute component={Home}/>
          <Route path={ '/about' } component={About} ></Route>
          <Route path={ '/user/:id' } component={User} ></Route>
          <Route exact={true} path={ '/' } component={Home} ></Route>
          {/* <Redirect to="/" /> */}
        </Route>
      </Router>
    );
  };
}

render(<CustomApp/> , document.querySelector( '#leApp' ) );