import React from 'react';

export class Home extends React.Component{
  render(){
    return(
      <div className="starter-template">
        <h1>Le Home Page</h1>
        <h2>Bootstrap starter template</h2>
        <p className="lead">Use this document as a way to quickly start any new project.<br /> All you get is this text and a mostly barebones HTML document.</p>
        <p className="lead">
          &nbsp; <span className="glyphicon glyphicon-pencil"></span>
          &nbsp; <span className="glyphicon glyphicon-envelope"></span>
          &nbsp; <span className="glyphicon glyphicon-search"></span>
          &nbsp; <span className="glyphicon glyphicon-heart"></span>
          &nbsp; <span className="glyphicon glyphicon-cloud"></span>
          &nbsp; <span className="glyphicon glyphicon-cloud-upload"></span>
          &nbsp; <span className="glyphicon glyphicon-cloud-download"></span>
        </p>
      </div>
    );
  };
}