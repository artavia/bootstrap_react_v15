import React from 'react';
import {Link} from 'react-router';

export const Header = (props) => {
  return(
    <nav className="navbar navbar-inverse navbar-fixed-top">
      <div className="container">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <Link className="navbar-brand" to={'/'}>don lucho</Link>
        </div>
        <div id="navbar" className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li><Link to={'/'} activeClassName={"active"} >Home</Link></li>
            <li><Link to={'/user/55'} activeStyle={{color: "#FF0099", fontWeight: "bold" }} >User</Link></li>
            <li><Link to={'/about'} activeClassName={"active"} >About</Link></li>
          </ul>
        </div>
      </div>
    </nav>
  );
};