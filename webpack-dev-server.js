
const webpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');

const config = require('./webpack.config.js');
const myport = 5150; // config.devServer.port; 
const myhostname = '127.1.8.12'; // config.devServer.host;  
const cb = () => { console.log(`Example app listening at http://${myhostname}:${myport}`); };

const options = {
  contentBase: './src'
  , inline: true
  , hot: true
  , host: myhostname // , host: '127.0.0.1'
  , port: myport // , port: 8080
  // , open: true
};

webpackDevServer.addDevServerEntrypoints( config, options );

const compiler = webpack(config);
const server = new webpackDevServer( compiler, options );

server.listen( myport, myhostname, cb ); // http://127.1.8.12:5150
