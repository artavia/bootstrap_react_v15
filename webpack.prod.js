"use strict";

const path = require("path");
const paths = {
  ROOT_DIR: path.resolve( __dirname ) // .
  , SRC_DIR: path.resolve( __dirname, 'src' ) // ./src
  , DIST_DIR: path.resolve( __dirname, 'dist' ) // ./dist
  , MODULES_DIR: path.resolve( __dirname, 'node_modules' ) // ./node_modules
};

const merge = require( 'webpack-merge' );
const common = require( './webpack.common.js' );

const webpack = require('webpack');

const wdefp = new webpack.DefinePlugin( {
  'process.env.NODE_ENV': JSON.stringify('production')
} );

const UglifyJSPlugin = require( 'uglifyjs-webpack-plugin' );
const ujsp = new UglifyJSPlugin({
  sourceMap: true
});

const config = {
  
  devtool: "source-map"

  , plugins: [ ujsp, wdefp ]
};

module.exports = merge(common,config);