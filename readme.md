# Description
An updated demo of how to import bootstrap-sass and use React nested routing and bundle, then, serve with Webpack and Webpack Dev Server.

## Please visit
You can see [the lesson at surge.sh](https://bootstrap_react_v15.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## Epilogue ~ 6/9/2018
Apparently, each the **serve** directive, the **expresso** directive, and the **finalrun** directive have all regressed while the others remain okay. Personally, it makes no difference to me because after all this experiment deals with **React Version 15**.

## Additional Repo
I have composed an accompanying repository that deals with [ReactJS version 16](https://gitlab.com/artavia/bootstrap_react_v16/ "link to companion repo"). I will just briefly make the comment that nested routes (and prop types) are different in modern or later versions of React depending on your perspective.

## My audience
If you are a neophyte, a rookie, or just a plain beginner, then, in the words of Van Halen in the beginning of &quot;Poundcake&quot; I can gladly state &quot;Boy, do I got some sh*t cooking for you!&quot; Come on in, get settled, and enjoy the ride.

## Caveat Emptor
These days Webpack has reached version four (4) with five (5) already on the way. You are going to notice differences in bundle optimization, and at a minimum differences between previous and present versions of the extract-text-webpack-plugin. &hellip;that is all!

## All inroads begin with Webpack
Coinciding with November or December 2017, I finally set out to learn a lot of programs including and not limited to [Webpack](https://webpack.js.org/ "link to Webpack"), [ReactJS](https://reactjs.org/ "link to ReactJS"), [Angular](https://angular.io/ "link to Angular"), and AWS. I could not visit Amazon Web Services and it has to wait because the free subscription will only last for one year. Boo! Thus, I decided to master the other software before embarking on AWS. Webpack is unique in the sense that you will find it goes well with each ReactJS and Angular. ng new myProject and create-react-app myProject can make development seem easy but once you decide to customize the configuration of an Angular project or eject a project in terms of ReactJS, then, the results can be overwhelming if you are not prepared. Enter Webpack.

## Scaffolding out the project structure
After running the obligatory npm init -y directive, I would create files with the touch command and establish directories with the mkdir command. Then, after drawing on experience and depending if I wanted to build a throwback or a modern version of a ReactJS project, I lumped in all of my library invocations into a concatenated statement. This should work out of the box for any of you but you are going to reach a point where you already have certain items in mind that you are itching to pull together ASAP. 

Presuming for a moment you are building from scratch and like I stated above, first I created the files and directories&hellip;:
```sh
$ mkdir bootstrap_react_v15 bootstrap_react_v15/src bootstrap_react_v15/src/app bootstrap_react_v15/src/app/{components,js,css} && touch bootstrap_react_v15/readme.md bootstrap_react_v15/webpack.config.js bootstrap_react_v15/.gitignore bootstrap_react_v15/src/index.html bootstrap_react_v15/src/app/app.js bootstrap_react_v15/src/app/vendorcode.js bootstrap_react_v15/src/app/js/index.js bootstrap_react_v15/src/app/css/{boots.scss,index.scss} bootstrap_react_v15/src/app/components/{Header.js,Home.js,Root.js,User.js,About.js}
```
&hellip;then, I imported the npm packages:
```sh
$ cd bootstrap_react_v15/ && npm init -y
$ npm install react@15.2.1 react-dom@15.2.1 react-router@2.6.1 bootstrap-sass jquery popper.js --save && npm install babel-core@6.16.0 babel-loader@6.2.4 babel-preset-env@0.0.9 babel-preset-react@6.11.1 clean-webpack-plugin@0.1.10 express@4.16.2 html-webpack-plugin@2.22.0 extract-text-webpack-plugin@3.0.2 node-sass@4.7.2 sass-loader@6.0.6 file-loader@1.1.5 css-loader@0.28.7 style-loader@0.19.0 uglifyjs-webpack-plugin@1.1.8 webpack-merge@4.1.1 webpack@3.9.1 webpack-dev-server@2.9.5 webpack-dev-middleware@2.0.4 webpack-hot-middleware@2.21.0 --save-dev 
```
## Who I learned from
In order to master Webpack (version 4 is the most current version at this present time), I needed to crawl before I would learn how to walk. There were a lot of pain points but I am better for the job these days because I put all my energies into it. I mean&hellip; what else am I going to do? I saw exercises in the context of old, archaic and often antiquated npm packages but I purposely set out to learn in current terms before doubling back to apply things in terms of yesteryear. Getting down to brass tacks, first, I would recommend Max Schwarz&#45;Mueller&rsquo;s video series on learning each [Webpack](https://www.youtube.com/playlist?list=PL55RiY5tL51rcCnrOrZixuOsZhAHHy6os "link to Webpack2 Basics series") and [ReactJs](https://www.youtube.com/playlist?list=PL55RiY5tL51oyA8euSROLjMFZbXaV7skS "link to ReactJS Basics series") if you are a total neophyte like I used to be. Also, you&rsquo;re going to need to be familiar with the different sections of the well&#45;documented official Webpack website such as the [concepts](https://webpack.js.org/concepts/ "link to the concepts section"), [configuration](https://webpack.js.org/configuration/ "link to the configuration section"), [api](https://webpack.js.org/api/ "link to the api section"), [guides](https://webpack.js.org/guides/ "link to the guides section"), [loaders](https://webpack.js.org/loaders/ "link to the loaders section"), and [plugins](https://webpack.js.org/plugins/ "link to the plugins section") sections&hellip; for starters! 

### Context behind my motivation
I used to visit [Bruce Lawson&rsquo;s personal blog](http://www.brucelawson.co.uk/ "link to Bruce Lawson&rsquo;s personal blog") a lot. I recall that only a few years ago, he could not stop singing the praises of everything ReactJS. After putting it off for a bit, I finally found an opportunity to get cracking. Presently, I have hardly had any time than for what is the item du-jour in front of me at any one time which is why I do not visit the blog that often. I will say that this exercise has more in common with the final React Routing exercise in the aforementioned ReactJS Basics series than with anything else. And, there are a lot of sources from which I drew on in order to complete this morsel of a demo. I will be the first to concede that I loathe the idea of connecting to a CDN for any reason. In the context of Webpack, css and sass are worthy of being fleshed out to a point where development is not dependent on an external content delivery network. In essence, this demonstration that you are visiting today expands on a very innocuous link found buried in the official documentation for [css-loader](https://webpack.js.org/loaders/css-loader/ "link to css-loader documentation"). When you visit that page you will be referred to a github repo that shows you how to get familiar with a working [bootstrap-sass](https://github.com/bbtfr/webpack2-bootstrap-sass-sample "link to bootstrap-sass example by bbtfr") example. It is very rudimentary in nature but the takeaway is universal in its application in terms of other future ReactJS projects. What a find! Theo Li&#45; you the man! And, so are you Maxxie! =)

### My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!